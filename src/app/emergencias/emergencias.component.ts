import { Component, OnInit } from '@angular/core';
import { emergencia } from '../emergencia';
import { emergenciaService } from '../emergencia.service';

@Component({
  selector: 'app-emergencias',
  templateUrl: './emergencias.component.html',
  styleUrls: ['./emergencias.component.css']
})

export class emergenciasComponent implements OnInit {
  emergencias: emergencia[];  
  selectedemergencia: emergencia;

  constructor(private emergenciaService: emergenciaService) { }
   
  ngOnInit() {  
   this.getemergencias();
  }
     

  onSelect(emergencia: emergencia): void {
    this.selectedemergencia = emergencia;
  }

  getemergencias(): void{
    this.emergenciaService.getemergencias()
    .subscribe(emergencias => this.emergencias = emergencias);
  }

  add(name: string): void {  //novo
    name = name.trim();
    if (!name) { return; }
    this.emergenciaService.addemergencia({ name } as emergencia)
      .subscribe(emergencia => {
        this.emergencias.push(emergencia);
      });
  }
 
  delete(emergencia: emergencia): void {
    this.emergencias = this.emergencias.filter(h => h !== emergencia);
    this.emergenciaService.deleteemergencia(emergencia).subscribe();
  }

}
