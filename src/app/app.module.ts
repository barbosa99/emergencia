import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; 
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { emergenciaDetailComponent } from './emergencia-detail/emergencia-detail.component';
import { emergenciasComponent } from './emergencias/emergencias.component';
import { EmergenciaSearchComponent } from './emergencia-search/emergencia-search.component';
import { MessagesComponent } from './messages/messages.component';

@NgModule({

  declarations: [
    AppComponent,
    DashboardComponent,
    emergenciasComponent,
    emergenciaDetailComponent,
    MessagesComponent,
    EmergenciaSearchComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

  // The HttpClientInMemoryWebApiModule module intercepts HTTP      requests
 // and returns simulated server responses.
 // Remove it when a real server is ready to receive requests.

 HttpClientInMemoryWebApiModule.forRoot(
  InMemoryDataService, { dataEncapsulation: false }
)
    
  ],

  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


