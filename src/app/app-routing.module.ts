import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { emergenciasComponent }  from './emergencias/emergencias.component';
import { emergenciaDetailComponent }  from './emergencia-detail/emergencia-detail.component';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: emergenciaDetailComponent },
  { path: 'emergencias', component: emergenciasComponent }
  
];

@NgModule({
  declarations: [],

 imports: [ RouterModule.forRoot(routes) ],
 exports: [ RouterModule ]
 
})
export class AppRoutingModule { }
