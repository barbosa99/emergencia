import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { emergenciaDetailComponent } from './emergencia-detail.component';

describe('EmergenciaDetailComponent', () => {
  let component: emergenciaDetailComponent;
  let fixture: ComponentFixture<emergenciaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ emergenciaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(emergenciaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
