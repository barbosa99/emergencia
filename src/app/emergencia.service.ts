import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { emergencia } from './emergencia';
import { emergencias } from './mock-emergencias';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class emergenciaService {

    private emergenciasUrl = 'api/heroes';  // URL to web api

  constructor(
  private http: HttpClient,
  private messageService: MessageService) { }


    getemergencias(): Observable<emergencia[]> {
     return this.http.get<emergencia[]>(this.emergenciasUrl)
  .pipe(
      tap(emergencias => this.log('fetched emergencias')),
      catchError(this.handleError('getemergencias', []))
       );
  }
 
    /** GET hero by id. Return `undefined` when id not found */
  getHeroNo404<Data>(id: number): Observable<emergencia> {
    const url = `${this.emergenciasUrl}/?id=${id}`;
    return this.http.get<emergencia[]>(url)
      .pipe(
        map(emergencias => emergencias[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} emergencia id=${id}`);
        }),
        catchError(this.handleError<emergencia>(`getemergencia id=${id}`))
      );
  }
 
  
  getemergencia(id: number): Observable<emergencia> {
    // TODO: send the message _after_ fetching the hero
     const url = `${this.emergenciasUrl}/${id}`;

    //this.messageService.add(`emergenciaService: fetched emergencia id=${id}`);
    //return of(emergencias.find(emergencia => emergencia.id === id));

    return this.http.get<emergencia>(url).pipe(
      tap(_ => this.log(`fetched emergencia id=${id}`)),
      catchError(this.handleError<emergencia>(`getemergencia id=${id}`))
    );
  
 }

  /* GET heroes whose name contains search term */
  searchemergencia(term: string): Observable<emergencia[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<emergencia[]>(`${this.emergenciasUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found emergencias matching "${term}"`)),
      catchError(this.handleError<emergencia[]>('searchemergencias', []))
    );
  }

//////// Save methods //////////
 
  /** POST: add a new hero to the server */
  addemergencia (emergencia: emergencia): Observable<emergencia> {
    return this.http.post<emergencia>(this.emergenciasUrl, emergencia, httpOptions).pipe(
      tap((emergencia: emergencia) => this.log(`added emergencia w/ id=${emergencia.id}`)),
      catchError(this.handleError<emergencia>('addemergencia'))
    );
  }

  /** DELETE: delete the hero from the server */
  deleteemergencia (emergencia: emergencia | number): Observable<emergencia> {
    const id = typeof emergencia === 'number' ? emergencia : emergencia.id;
    const url = `${this.emergenciasUrl}/${id}`;
 
    return this.http.delete<emergencia>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted emergencia id=${id}`)),
      catchError(this.handleError<emergencia>('deleteemergencia'))
    );
  }

  /** PUT: update the hero on the server */
  updateemergencia (emergencia: emergencia): Observable<any> {
    return this.http.put(this.emergenciasUrl, emergencia, httpOptions).pipe(
      tap(_ => this.log(`updated emergencia id=${emergencia.id}`)),
      catchError(this.handleError<any>('updateemergencia'))
    );
  }

/**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

 /** Log a HeroService message with the MessageService */
 private log(message: string) {
  this.messageService.add(`emergenciaService: ${message}`);
}
}
