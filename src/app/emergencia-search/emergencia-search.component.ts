import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import { emergencia } from '../emergencia';
import { emergenciaService } from '../emergencia.service';

@Component({
  selector: 'app-emergencia-search',
  templateUrl: './emergencia-search.component.html',
  styleUrls: ['./emergencia-search.component.css']
})
export class EmergenciaSearchComponent implements OnInit {
    emergencias$: Observable<emergencia[]>;
    private searchTerms = new Subject<string>();

  constructor(private emergenciaService: emergenciaService) { }

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }
  

  ngOnInit(): void {
  
      this.emergencias$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
 
      // switch to new search observable each time the term changes
      switchMap((term: string) =>   this.emergenciaService.searchemergencia(term)),
    );

  }

}
