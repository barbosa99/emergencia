import { TestBed } from '@angular/core/testing';

import { emergenciaService } from './emergencia.service';

describe('EmergenciaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: emergenciaService = TestBed.get(emergenciaService);
    expect(service).toBeTruthy();
  });
});
